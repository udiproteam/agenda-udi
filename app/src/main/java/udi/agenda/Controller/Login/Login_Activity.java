package udi.agenda.Controller.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import udi.agenda.Controller.Tarea.MainActivity;
import udi.agenda.Model.UTILS.Preferences;
import udi.agenda.R;

public class Login_Activity extends AppCompatActivity {
    SharedPreferences prefsLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefsLogin = getSharedPreferences("PrefsLogin", Context.MODE_PRIVATE);
    }
    public void RunSession(){
        String nombreUsuario = Preferences.getUserPrefs(prefsLogin);
        String password = Preferences.getPassPreferences(prefsLogin);

        Intent menuIntent = new Intent(this,
                MainActivity.class);


        if (!TextUtils.isEmpty(nombreUsuario) && !TextUtils.isEmpty(password)){
            startActivity(menuIntent);
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);//trnascion de activiti
            Login_Activity.this.finish();
        }


    }

    public void SaveOnPreferences(String usernameText, String pass, int IdUsuario, String nombreUsuario,String tipoUsuario) {
        SharedPreferences.Editor editor = prefsLogin.edit();
        editor.putString("usernameText", usernameText);
        editor.putString("password", pass);
        editor.putInt("idUsuario", IdUsuario);
        editor.putString("nombreUsuario", nombreUsuario);
        editor.putString("tipoUsuario", tipoUsuario);
        editor.apply();

    }

    public void Login(View view) {
        String usr = ((EditText)findViewById(R.id.idUserEmail)).getText().toString();
        String pwd = ((EditText)findViewById(R.id.idUserPwd)).getText().toString();

        TextView message = findViewById(R.id.idMessage);

        if (usr.isEmpty() || pwd.isEmpty()) {
            message.setVisibility(View.VISIBLE);
            return;
        }


        message.setVisibility(View.INVISIBLE);

        startActivity(new Intent(this, MainActivity.class));
    }

    public void GoRegistro(View view) {
        startActivity(new Intent(this, RegistroLogin_Activity.class));
    }
}
