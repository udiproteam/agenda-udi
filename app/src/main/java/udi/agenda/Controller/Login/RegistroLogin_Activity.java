package udi.agenda.Controller.Login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import udi.agenda.Controller.FIREBASE.FirebaseController;
import udi.agenda.Model.ClsUsuario;
import udi.agenda.Model.UTILS.AlertaDialog;
import udi.agenda.R;

public class RegistroLogin_Activity extends AppCompatActivity {
    AlertaDialog alertaDialog;
    EditText txtnombre,txtapellido,txtnombreU,txtPassword,txtPassword2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrologin);
        initComponets();
    }
    private void initComponets(){
        alertaDialog = new AlertaDialog();

        txtnombre = (EditText)findViewById(R.id.Reg_Nombre);
        txtapellido = (EditText)findViewById(R.id.Reg_Apellido);
        txtnombreU = (EditText)findViewById(R.id.Reg_NomUser);
        txtPassword = (EditText)findViewById(R.id.Reg_Contra);
        txtPassword2 = (EditText)findViewById(R.id.Reg_ContraRep);
    }
    public boolean validarDatos(){
        if (txtnombre.getText().toString().equals("")){
            Toast.makeText(this, "El campo de nombre no puede ir Vacio", Toast.LENGTH_SHORT).show();
            return false;
        }else if (txtnombre.getText().toString().length()<4){
            Toast.makeText(this, "El nombre debe tener Al menos 3 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (txtapellido.getText().toString().equals("")){
            Toast.makeText(this, "El campo de apellido no puede ir Vacio", Toast.LENGTH_SHORT).show();
            return false;
        }else if (txtapellido.getText().toString().length()<4){
            Toast.makeText(this, "El apellido debe tener Al menos 3 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (txtnombreU.getText().toString().equals("")){
            Toast.makeText(this, "El campo de Nombre Usuario no puede ir Vacio", Toast.LENGTH_SHORT).show();
            return false;
        }else if (txtnombreU.getText().toString().length()<5){
            Toast.makeText(this, "El Nombre Usuario debe tener Al menos 5 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (txtPassword.getText().toString().equals("")){
            Toast.makeText(this, "El campo de Contraceña no puede ir Vacio", Toast.LENGTH_SHORT).show();
            return false;
        }else if (txtPassword.getText().toString().length()<8){
            Toast.makeText(this, "El campo de Contraceña debe tener Al menos 8 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!txtPassword.getText().toString().equals(txtPassword2.getText().toString())){
            Toast.makeText(this, "Error revise Las Contraceñas no coinciden", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void IniciarRegistro(View view) {
        if(validarDatos()==true){
            ClsUsuario objusuario = new ClsUsuario(txtnombre.getText().toString(),txtapellido.getText().toString(),txtnombreU.getText().toString(),txtPassword.getText().toString());
            CreateUser(objusuario);
        }
    }
    public void CreateUser(ClsUsuario clsUsuario){
        //this.id = UUID.randomUUID().toString();
        FirebaseController firebaseController = new FirebaseController(this);
        firebaseController.getDatabaseReference().child("Usuario").child(clsUsuario.getId()).setValue(clsUsuario, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError != null) {
                    alertaDialog.ShowAlertDialog(RegistroLogin_Activity.this,"Data could not be saved ",databaseError.getMessage());
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                } else {
                    alertaDialog.ShowAlertDialog(RegistroLogin_Activity.this,"Alerta","Registro Exitoso");
                }

            }
        });
    }
}

