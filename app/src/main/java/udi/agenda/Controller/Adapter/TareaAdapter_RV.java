package udi.agenda.Controller.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import udi.agenda.Model.ClsTarea;
import udi.agenda.Interface.RecyclerView_ClickListener;
import udi.agenda.R;


public class TareaAdapter_RV extends RecyclerView.Adapter<TareaAdapter_RV.ViewHolder>{
    public Context context;
    public List<ClsTarea> ListaAgenda;
    private RecyclerView_ClickListener clickListener;
    public TareaAdapter_RV(Context context, List<ClsTarea> catList){
        this.context = context;
        this.ListaAgenda = catList;
    }

    @Override
    public TareaAdapter_RV.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tarea, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.Titulo.setText(ListaAgenda.get(position).getNombre());
        holder.Fecha.setText(ListaAgenda.get(position).getFecha());
        /*if(ListaAgenda.get(position).getEstado()){
            holder.Estado.setText("Disponible");
        }else{holder.Estado.setText("Ocupado");
        }*/
        holder.Descripcion.setText(ListaAgenda.get(position).getDescripcion());
    }

    @Override
    public int getItemCount() {
        return ListaAgenda.size();
    }
    public void setListTarea(List<ClsTarea> ltarea){
        ListaAgenda = ltarea;
        this.notifyDataSetChanged();
    }

    public void setClickListener(RecyclerView_ClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView Titulo,Fecha;
        public TextView Descripcion;
        public CardView contenedor;
        public ViewHolder(View itemView) {
            super(itemView);

            Titulo = (TextView) itemView.findViewById(R.id.txtTitulo);
            Fecha = (TextView) itemView.findViewById(R.id.txtFecha);
            Descripcion = (TextView) itemView.findViewById(R.id.etdescripcion);
            contenedor = (CardView) itemView.findViewById(R.id.flcontainer);
            contenedor.setTag(contenedor);
            contenedor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
