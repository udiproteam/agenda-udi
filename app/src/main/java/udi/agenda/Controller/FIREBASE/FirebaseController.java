package udi.agenda.Controller.FIREBASE;

import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseController {

    private static FirebaseDatabase firebaseDatabase;
    private static DatabaseReference databaseReference;

    Context context;
    public  FirebaseController(Context context){
        this.context = context;
        initFirebase();
    }
    public void initFirebase(){
        FirebaseApp.initializeApp(context);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    public static DatabaseReference getDatabaseReference() {
        return databaseReference;
    }
}
