package udi.agenda.Controller.Tarea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import udi.agenda.R;

public class DetalleSubTarea extends AppCompatActivity {

    private String IdTarea,NombreTarea;
    private EditText etsubTarea;
    private TextView txttitulo;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detallesubtarea);

        InitComponent();
        InitView();
    }
    private void InitComponent(){

        IdTarea = (getIntent() != null) ?  getIntent().getExtras().getString("IdTarea") : "" ;
        NombreTarea = (getIntent() != null) ?  getIntent().getExtras().getString("TituloTarea") : "" ;
        txttitulo = (TextView)findViewById(R.id.txtdsbTitulo);
        etsubTarea = (EditText)findViewById(R.id.etsbSubtarea);
    }
    private void InitView(){
        txttitulo.setText(NombreTarea);
    }
    private void LoadRecyclerView(){
        recyclerView = (RecyclerView)findViewById(R.id.rvDetalleAgenda);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
      //  adapterAgenda =  new TareaAdapter_RV(this,listaAgenda);
     //   recyclerView.setAdapter(adapterAgenda);
        //adapterAgenda.setClickListener(this);
    }

    public void AddSubTarea(View view) {
    }
}
