package udi.agenda.Controller.Tarea;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONStringer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import udi.agenda.Controller.DatePickerFragment;
import udi.agenda.Controller.FIREBASE.FirebaseController;
import udi.agenda.Model.ClsTarea;
import udi.agenda.R;

import static udi.agenda.Controller.Tarea.MainActivity.JSON;

public class CreateTarea_Activity extends AppCompatActivity {


    FirebaseController firebaseController;
    DatabaseReference databaseReference;
    private  EditText etFecha,etTitulo,etDescripcion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createtarea);
        initComponents();
        initFirebase();
    }
    public void initComponents(){
        etDescripcion =(EditText) findViewById(R.id.CTtxtDescripcion);
        etTitulo =(EditText) findViewById(R.id.CTtxtTiulo);
        etFecha = (EditText) findViewById(R.id.CTetFecha);
        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });
    }
    public void initFirebase(){
        firebaseController = new FirebaseController(this);
    }
    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero
                final String selectedDate = day + "/" + (month+1) + "/" + year;
                etFecha.setText(selectedDate);
            }
        });
        newFragment.show(this.getFragmentManager(), "datePicker");
    }

    public void CreateTareaBtnAgregar(View view) {
        ClsTarea objAgenda = new ClsTarea();
        objAgenda.setId(UUID.randomUUID().toString());
        objAgenda.setDescripcion(etDescripcion.getText().toString());
        objAgenda.setNombre(etTitulo.getText().toString());
        objAgenda.setFecha(etFecha.getText().toString());
        objAgenda.setEstado(false);
        SendhttpRequestt(objAgenda);
        //SendFireBase(objAgenda);
    }
    public void  SendhttpRequestt(ClsTarea tarea){
        Gson gson = new Gson();
        String json = gson.toJson(tarea);
        requestPost("http://192.168.43.225:50504/api/Tarea/SETtarea", json, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseSTr = response.body().string();
                Gson gson = new Gson();
                Type listType = new TypeToken<List<ClsTarea>>(){}.getType();
               // List<ClsTarea> tareas = (List<ClsTarea>)gson.fromJson(responseSTr,listType);

            }
        });
    }
    public void SendFireBase(ClsTarea objAgenda){
        firebaseController.getDatabaseReference().child("Tarea").child(objAgenda.getId()).setValue(objAgenda, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError != null) {
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                } else {
                    ShowAlertDialog();
                }

            }
        });
    }
    OkHttpClient client = new OkHttpClient();
    private Call requestPost(String url, String json, Callback callback){
        RequestBody body = RequestBody.create(MediaType
                .parse("application/json"),json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return  call;
    }
    public void ShowAlertDialog(){
        
    }
}
