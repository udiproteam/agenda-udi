package udi.agenda.Controller.Tarea;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import udi.agenda.Controller.Adapter.TareaAdapter_RV;
import udi.agenda.Controller.FIREBASE.FirebaseController;
import udi.agenda.Model.ClsTarea;
import udi.agenda.Interface.RecyclerView_ClickListener;
import udi.agenda.R;

public class MainActivity extends AppCompatActivity implements RecyclerView_ClickListener {

    private RecyclerView DetalleAgendaRV;
    private TareaAdapter_RV adapterAgenda;
    private List<ClsTarea> listaAgenda;
    private FirebaseController firebaseController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goCreateTarea();
            }
        });
        initFirebase();
        listaAgenda =  new ArrayList<>();
        this.LoadRecyclerView();
        this.CreateDataAgendawRequest();
        //this.CreateDataAgenda();FIREBASE
    }
    public void initFirebase(){
        firebaseController = new FirebaseController(this);
    }

    public void goCreateTarea(){
        startActivity(new Intent(this,CreateTarea_Activity.class));
    }

    private void CreateDataAgendawRequest(){

//        requestGet("http://192.168.137.178:50504/api/Tarea/tarea", new Callback() {
          requestGet("http://192.168.43.225:50504/api/Tarea/tarea", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String responseSTr = response.body().string();
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<ClsTarea>>(){}.getType();
                    List<ClsTarea> tareas = (List<ClsTarea>)gson.fromJson(responseSTr,listType);
                    listaAgenda = tareas;
                    loadChageTable();
                }
            }

        });
    }
    private void loadChageTable(){
        runOnUiThread(new Runnable() {
            public void run() {
                adapterAgenda.setListTarea(listaAgenda);
            }
        });


       // adapterAgenda.notifyDataSetChanged();
    }
    private void CreateDataAgenda(){
        /*for (int i = 0; i < 10; i++) {
            listaAgenda.add(new ClsTarea("Tarea "+i ,"Task "+i+" muy importante no olvidar",  "28/0"+i+"/2018"));
        }*/
        firebaseController.getDatabaseReference().child("Tarea").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ClsTarea tarea = dataSnapshot.getValue(ClsTarea.class);
                listaAgenda.add(tarea);
                adapterAgenda.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    OkHttpClient client = new OkHttpClient();
    public static  final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private Call requestGet(String url, Callback callback){
        Request request = new Request.Builder()
                              .url(url)
                              .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return  call;
    }
    private Call requestPost(String url,String json,Callback callback){
        RequestBody body = RequestBody.create(JSON ,json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return  call;
    }
    private void LoadRecyclerView(){
        DetalleAgendaRV = (RecyclerView)findViewById(R.id.rvDetalleAgenda);
        DetalleAgendaRV.setLayoutManager(new LinearLayoutManager(this));
        adapterAgenda =  new TareaAdapter_RV(this,listaAgenda);
        DetalleAgendaRV.setAdapter(adapterAgenda);
        adapterAgenda.setClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, int position) {
        ClsTarea task = listaAgenda.get(position);
        Intent intent = new Intent(this,DetalleSubTarea.class);
        intent.putExtra("IdTarea",task.getId());
        intent.putExtra("TituloTarea",task.getNombre());
        startActivity(intent);

    }
}
