package udi.agenda.Model.UTILS;

import android.content.SharedPreferences;

/**
 * Created by LUIS PEREZ on 15/9/2018.
 */

public class Preferences {


    public static String getUserPrefs(SharedPreferences preferences) {
        return preferences.getString("usernameText", "");
    }

    public static String getPassPreferences(SharedPreferences preferences) {
        return preferences.getString("password", "");
    }

    public static String getNombreUsuario(SharedPreferences preferences) {
        return preferences.getString("nombreUsuario", "");
    }

    public static Integer getIdUsuario(SharedPreferences preferences) {
        return preferences.getInt("idUsuario", 0);
    }
    public static String getTipoUsuario(SharedPreferences preferences) {
        return preferences.getString("tipoUsuario", "");
    }

}
