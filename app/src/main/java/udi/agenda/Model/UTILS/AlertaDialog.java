package udi.agenda.Model.UTILS;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import udi.agenda.R;

public class AlertaDialog {
    public void ShowAlertDialog(Context context, String Titulo, String Detalle){
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                context, R.style.AppThemes).create();

        // Setting Dialog Title
        alertDialog.setTitle(Titulo);

        // Setting Dialog Message
        alertDialog.setMessage(Detalle);

        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.tick);
       // alertDialog.setB
        // Setting OK Button
        alertDialog.setButton(Dialog.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
