package udi.agenda.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClsTarea {
    private String id;
    private String nombre;
    private String description;
    private boolean estado;
    private String  fecha;

    public ClsTarea(){
        id= "";
        nombre= "";
        description = "";
        fecha = "";
        estado = false;
    }
    public ClsTarea(String nombre, String descripcion, String fecha) {
        nombre = nombre;
        descripcion = descripcion;
        fecha = fecha;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return description;
    }

    public void setDescripcion(String descripcion) {
        this.description = descripcion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
