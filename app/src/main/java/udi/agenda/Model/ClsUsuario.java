package udi.agenda.Model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.Map;
import java.util.UUID;

import udi.agenda.Controller.FIREBASE.FirebaseController;
import udi.agenda.Model.UTILS.AlertaDialog;

public class ClsUsuario {
    private String id;
    private String nombre;
    private String apellido;
    private String nUsuario;
    private String contracena;
    private Map<String,Boolean> idAgenda;

    public ClsUsuario(String nombre, String apellido, String nUsuario, String contracena) {
        this.id = UUID.randomUUID().toString();
        this.nombre = nombre;
        this.apellido = apellido;
        this.nUsuario = nUsuario;
        this.contracena = contracena;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getnUsuario() {
        return nUsuario;
    }

    public void setnUsuario(String nUsuario) {
        this.nUsuario = nUsuario;
    }

    public String getContracena() {
        return contracena;
    }

    public void setContracena(String contracena) {
        this.contracena = contracena;
    }

    public Map<String, Boolean> getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(Map<String, Boolean> idAgenda) {
        this.idAgenda = idAgenda;
    }
}
